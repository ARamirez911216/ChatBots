#!/bin/bash
#ASIMOV_bashBot
#Author: Angel Arturo Ramirez Suarez
#Description: a simple chatbot made using bash.

userInput=START
#Repeat until loop is finished.
while [ $userInput!=END ]
do
	#Read a string from the user.
	read -p "Hola, ¿como te llamas? " userInput
	#Parse it and give back the bot answer.
	if [ $userInput != END ]
	then
		echo Es un gusto conocerte, $userInput
	#elif [ $userInput = END ]
	elif [ $userInput = END ]
	then
		break
	fi
done
date